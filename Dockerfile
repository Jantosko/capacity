FROM mhart/alpine-node

RUN mkdir /app
COPY package.json /app/package.json
COPY capacity.js /app/capacity.js
COPY .babelrc /app/.babelrc

RUN cd /app && npm install

ENTRYPOINT [ "node", "/app/node_modules/babel-cli/bin/babel-node.js", "/app/capacity.js" ]
