'use strict';

import _ from 'lodash';

const DISTRIBUTION = {
    //1.6 - 15.6. (2weeks)
    1: 255344,
    2: 76629,
    3: 36700,
    4: 21863,
    5: 26853,
    7: 22884,
    10: 21887,
    15: 14113,
    20: 24984,
    35: 14659,
    50: 15389,
    75: 9984,
    100: 45030
    /*
    //6.6 - 12.6. (1week)
    1: 145184,
    2: 42791,
    3: 21018,
    4: 12987,
    5: 16412,
    7: 14547,
    10: 14613,
    15: 9931,
    20: 17865,
    35: 10727,
    50: 10982,
    75: 6934,
    100: 23555,
    */
};

const CAPPING = parseInt(process.argv[2], 10);
const IMPRESSIONS = parseInt(process.argv[3], 10);

console.log('Using capping ' + CAPPING + ' and impressions ' + IMPRESSIONS);

const cache = {};

/*
 * Generate all possible sum participants for number
 *
 * E.g.: for 3 -> [[1, 1, 1], [1, 2], [3]]
 */
function generateSums(num, capping) {
    if (cache[`${num}-${capping}`]) {
        return cache[`${num}-${capping}`];
    }

    let memory = [];
    function recSums(current) {
        if (!current.length) {
            return current;
        }

        let clone = current.slice(0);
        let val = clone.shift();

        let res = [];
        for (let i = 0; i < clone.length; i++) {
            let c = clone.slice(0);
            let cur = c[i] += val;
            c.sort();
            // Speed up
            if (cur > capping || c[0] > capping || memory.indexOf(c.join('')) > -1) {
                continue;
            }
            memory.push(c.join(''));
            res.push(c);
            res = res.concat(recSums(c));
        }
        return res;
    }

    let ones = [];
    for (let i = 0; i < num; i++) {
        ones.push(1);
    }
    let sums = _.uniqWith([ones].concat(recSums(ones)).map(v => v.sort()), _.isEqual);
    cache[`${num}-${capping}`] = sums;
    return sums;
}

/**
 * Generate all possible setups for distribution
 */
function generateOptions(distribution, keys, capping, current = {}) {
    function genOptions(pageviews, users) {
        if (cache[`${pageviews}-${capping}-${users}`]) {
            return cache[`${pageviews}-${capping}-${users}`];
        }

        // Limit by capping
        // console.log(generateSums(pageviews));
        let options = generateSums(pageviews, capping).map(sum => sum.map(s => s * users));
        cache[`${pageviews}-${capping}-${users}`] = options;
        return options;
    }

    if (!keys.length) {
        return summarize(current, IMPRESSIONS);
    }

    let combinations = [];
    let clonedKeys = keys.slice(0);
    let key = clonedKeys.pop();
    // TODO: This is a lot of memory - cache somehow or reuse existing objects?
    let options = genOptions(key, distribution[key]).map(o => {
        return {
            ...current,
            [key]: o
        };
    });

    // TODO: This is a lot of memory - cache somehow or reuse existing objects?
    combinations = combinations.concat(
        _.flatten(options.map(o => generateOptions(distribution, clonedKeys, capping, o)))
    );

    return combinations;
}

/**
 * Find how many campaigns is possible in the given option
 */
function summarize(option, capacity) {
    let usersPerPageviews = _.cloneDeep(_.values(option));
    // console.log(JSON.stringify(usersPerPageviews));
    let result = 0;
    let needToTake;
    let group;

    // Calc from end
    while (usersPerPageviews.length) {
        let usersPerPageview = usersPerPageviews.pop();
        while (usersPerPageview.length) {
            group = usersPerPageview.pop();
            // If capacity is less then in the group - take modulo
            if (group > capacity) {
                result += Math.floor(group / capacity);
                group = group % capacity;
            }

            if (group === 0) { // Skip if empty group
                continue;
            }

            needToTake = capacity - group;
            let i = usersPerPageviews.length;
            while (needToTake > 0 && i > 0) {
                // console.log(needToTake, usersPerPageviews, usersPerPageview);
                // Countdown
                i -= 1;

                let upp = usersPerPageviews[i];
                if (!upp.length) {
                    continue;
                }

                let last = upp[upp.length - 1];

                if (last >= needToTake) {
                    upp[upp.length - 1] -= needToTake;
                    needToTake = 0;
                } else {
                    needToTake -= last;
                    upp.pop();
                }
            }

            // console.log(needToTake, usersPerPageviews, usersPerPageview);

            if (needToTake === 0) {
                result++;
            }
        }
    }
    // console.log('last', group);
    return result;
}

// let options = generateOptions(DISTRIBUTION, Object.keys(DISTRIBUTION), CAPPING);
// console.log(_.max(options));
// let campaigns = options.map(option => summarize(option, IMPRESSIONS));
// console.log(_.max(campaigns));


function greedy_max_campaigns(capping, impressions) {
    let CAPACITY = new Array(101).fill(0);
    for (let pv in DISTRIBUTION) CAPACITY[parseInt(pv,10)] = DISTRIBUTION[pv]
    //console.log(capacity_prettyprint(CAPACITY));

    let num_campaigns = 0;
    do {
        let REMAINING_CAPACITY = new Array(101).fill(0);
        let CAMPAIGN = new Array(101).fill(0);
        let campaign_impressions = 0;
        for (let pv = CAPACITY.length - 1; pv > 0; pv--) {
            if (campaign_impressions >= impressions) {
                REMAINING_CAPACITY[pv] += CAPACITY[pv];
                continue;
            }

            let available_users = CAPACITY[pv];
            if (available_users == 0)
                continue;

            let impressions_per_user = Math.min(pv, capping);
            let needed_impressions = impressions - campaign_impressions;
            let used_users = Math.min(available_users, Math.ceil(needed_impressions / impressions_per_user));

            REMAINING_CAPACITY[pv] += available_users - used_users;
            if (pv - impressions_per_user > 0)
                REMAINING_CAPACITY[pv - impressions_per_user] += used_users;

            CAMPAIGN[pv] = used_users;
            campaign_impressions += used_users * impressions_per_user;
        }

        if (campaign_impressions >= impressions) {
            //found campaign
            num_campaigns++;
        } else {
            break;
        }
        CAPACITY = REMAINING_CAPACITY;

        //console.log(num_campaigns, campaign_impressions, JSON.stringify(capacity_prettyprint(REMAINING_CAPACITY)));

    }
    while (true);
    console.log(`${impressions}, ${capping}, ${num_campaigns}`);
}

function capacity_prettyprint(cap) {
    let pcap = _.fromPairs(_.filter(_.map(cap, (val, idx) => [idx, val]), p => p[1]>0));
    return pcap;
}

for (let cap = 2; cap<=7; cap++) {
    for (let impr = 350; impr<=1000; impr+=50) {
        greedy_max_campaigns(cap, impr*1000)
    }
}
